from kivy.uix.button import Button
from kivy.app import App
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.screenmanager import Screen, ScreenManager
from random import shuffle
import time

COLOR_LIST = [  (1,0,0,1),
                (0,1,0,1),
                (0,0,1,1),
                (1,1,0,1),
                (1,0,1,1),
                (0,1,1,1),
                (0.5,0.5,0.5,1),
                (0,0,0,1)]

class Appli(App):
    def build(self):
        return ScreenChanger()

class Tile(Button):
    def __init__(self, color, grid,index,**kwargs):
        self.default_color = (1,1,1,1)
        self.color = color
        self.grid = grid
        self.index = index
        self.status = 0
        super().__init__(**kwargs)
        self.background_down = 'atlas://data/images/defaulttheme/button'
        self.background_color = self.default_color
       
    def on_press(self):
        self.background_color = self.color
    def on_release(self):
        if self.status == 0:
            if self.index not in self.grid.get_flipped_tiles():
                self.grid.inform_grid(self.index)

    def get_index(self):
        return self.index

    def get_color(self):
        return self.color

    def unflip(self):
        self.background_color = (1,1,1,1)
    
    def setFound(self):
        self.status = 1

class Grid(Screen):
    def __init__(self, **kwargs):
        self.mainWidget = GridLayout(cols=4, rows=4)
        self.listTiles = [Tile(COLOR_LIST[i//2],self,i) for i in range(16)]
        shuffle(self.listTiles)
        self.status = "vide"
        self.flippedTiles = []
        self.foundPairs = 0
        super().__init__(**kwargs)
        for tile in self.listTiles:
            self.mainWidget.add_widget(tile)
            
        self.add_widget(self.mainWidget)
    def inform_grid(self, index=None):
        if self.status == "vide":
            self.status = "une_retournee"
            self.flippedTiles.append(index)
            print(self.status)
        elif self.status == "une_retournee":
            self.status = "deux_retournees"
            self.flippedTiles.append(index)
            self.inform_grid()
            print(self.status)
        elif self.status == "deux_retournees":
            currentTiles = []
            for tile in self.listTiles:
                if tile.get_index() in self.flippedTiles:
                    currentTiles.append(tile)
            if currentTiles[0].get_color() != currentTiles[1].get_color():
                time.sleep(1)
                for tile in currentTiles:
                    tile.unflip()
            else:
                self.foundPairs += 1
                currentTiles[0].setFound()
                currentTiles[1].setFound()
                if self.foundPairs == 8:
                    self.status = "fini"
                    self.inform_grid()
            self.status = "vide"
            self.flippedTiles = []
        elif self.status == "fini":
            self.manager.current = "end"
        
    def get_flipped_tiles(self):
        return self.flippedTiles

    def replay(self):
        self.clear_widgets()
        self.mainWidget = GridLayout(cols=4, rows=4)
        self.listTiles = [Tile(COLOR_LIST[i//2],self,i) for i in range(16)]
        shuffle(self.listTiles)
        self.status = "vide"
        self.flippedTiles = []
        self.foundPairs = 0
        self.manager.current = 'game'
        for tile in self.listTiles:
            self.mainWidget.add_widget(tile)
            
        self.add_widget(self.mainWidget)

class BeginScreen(Screen):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.main_widget = BoxLayout(spacing=10, padding=200, orientation="vertical")
        self.main_widget.add_widget(Label(text="Choississez le nombre de paires"))
        self.button = Button(text="8", i=4, j=4)
        self.button = Button(text="10", i=5, j=4)
        self.button = Button(text="15", i=5, j=6)
        self.button.bind(on_press = self.replay)
        self.main_widget.add_widget(self.button)
        self.add_widget(self.main_widget)


class EndScreen(Screen):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.main_widget = BoxLayout(spacing=10, padding=200, orientation="vertical")
        self.main_widget.add_widget(Label(text="Vous avez gagné !"))
        self.button = Button(text="Rejouer",pos_hint={'x':.4, 'y':.6}, size_hint=(.2,.3))
        self.button.bind(on_press = self.replay)
        self.main_widget.add_widget(self.button)
        self.add_widget(self.main_widget)

    def replay(self,button):
        self.manager.get_screen('game').replay()


class ScreenChanger(ScreenManager):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.gameScreen = Grid(name="game")
        self.endScreen = EndScreen(name="end")
        self.add_widget(self.gameScreen)
        self.add_widget(self.endScreen)


if __name__ == "__main__":
    Appli().run()