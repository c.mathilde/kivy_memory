# Kivy Memory 

Facultatif : 
Installer un venv et le lancer :
- virtualenv -p python3 venv-kivy 
- source venv-kivy/bin/activate

Etapes à suivre : 
- git clone https://gitlab.com/c.mathilde/kivy_memory.git
- Se déplacer dans le dossier enfant
- Faire un pip install -r requirements.txt
- python3 game.py